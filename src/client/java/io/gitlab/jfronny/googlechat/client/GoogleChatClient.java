package io.gitlab.jfronny.googlechat.client;

import com.mojang.brigadier.context.CommandContextBuilder;
import com.mojang.brigadier.context.ParsedArgument;
import com.mojang.brigadier.context.ParsedCommandNode;
import com.mojang.brigadier.tree.ArgumentCommandNode;
import com.mojang.brigadier.tree.LiteralCommandNode;
import com.mojang.brigadier.tree.RootCommandNode;
import io.gitlab.jfronny.googlechat.GoogleChat;
import io.gitlab.jfronny.googlechat.GoogleChatConfig;
import io.gitlab.jfronny.libjf.config.api.v2.ConfigHolder;
import io.gitlab.jfronny.libjf.config.api.v2.ConfigInstance;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.client.option.KeyBinding;
import net.minecraft.client.util.InputUtil;
import net.minecraft.command.CommandSource;
import net.minecraft.command.argument.MessageArgumentType;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

public class GoogleChatClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        ConfigInstance ci = ConfigHolder.getInstance().get(GoogleChat.MOD_ID);
        if (ci != null
                && FabricLoader.getInstance().isModLoaded("fabric-key-binding-api-v1")
                && FabricLoader.getInstance().isModLoaded("fabric-lifecycle-events-v1")) {
            setupKeybind(ci);
        }
    }

    private static void setupKeybind(@NotNull ConfigInstance ci) {
        // Factored out to prevent loading classes if mods are not present
        KeyBinding keyBinding = KeyBindingHelper.registerKeyBinding(new KeyBinding(
                "key." + GoogleChat.MOD_ID + ".toggle",
                InputUtil.Type.KEYSYM,
                -1,
                KeyBinding.MULTIPLAYER_CATEGORY
        ));
        ClientTickEvents.END_CLIENT_TICK.register(client -> {
            if (keyBinding.wasPressed()) {
                GoogleChatConfig.General.enabled = !GoogleChatConfig.General.enabled;
                ci.write();
            }
        });
    }

    public static Optional<String> reconstitute(String source, CommandContextBuilder<CommandSource> results) {
        StringBuilder builder = new StringBuilder();
        for (ParsedCommandNode<CommandSource> node : results.getNodes()) {
            switch (node.getNode()) {
                case ArgumentCommandNode<?, ?> arg -> {
                    ParsedArgument<CommandSource, ?> pa = results.getArguments().get(arg.getName());
                    String datum = pa.getRange().get(source);
                    if (pa.getResult() instanceof MessageArgumentType.MessageFormat fmt) builder.append(fmt.contents());
                    else builder.append(datum);
                    builder.append(' ');
                }
                case LiteralCommandNode<?> lit -> {
                    builder.append(lit.getLiteral());
                    builder.append(' ');
                }
                case RootCommandNode<?> root -> {}
                default -> {
                    return Optional.empty();
                }
            }
        }
        if (GoogleChatConfig.Advanced.debugLogs) {
            GoogleChat.LOGGER.info("Reconstituted command: {0} from {1}", builder.substring(0, builder.length() - 1), source);
        }
        return Optional.of(builder.substring(0, builder.length() - 1));
    }
}
