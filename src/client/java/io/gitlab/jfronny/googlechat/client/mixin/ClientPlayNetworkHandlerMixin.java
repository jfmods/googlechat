package io.gitlab.jfronny.googlechat.client.mixin;

import com.mojang.brigadier.ParseResults;
import com.mojang.brigadier.context.ParsedArgument;
import io.gitlab.jfronny.googlechat.GoogleChat;
import io.gitlab.jfronny.googlechat.GoogleChatConfig;
import io.gitlab.jfronny.googlechat.TranslationDirection;
import io.gitlab.jfronny.googlechat.client.GoogleChatClient;
import net.minecraft.client.network.ClientPlayNetworkHandler;
import net.minecraft.command.CommandSource;
import net.minecraft.command.argument.MessageArgumentType;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyVariable;
import org.spongepowered.asm.mixin.injection.Redirect;

import java.util.concurrent.atomic.AtomicReference;

@Mixin(ClientPlayNetworkHandler.class)
public class ClientPlayNetworkHandlerMixin {
    @Shadow private ParseResults<CommandSource> parse(String command) {
        throw new IllegalStateException("Mixin failed to apply");
    }

    @Unique private final AtomicReference<ParseResults<CommandSource>> googlechat$lastResults = new AtomicReference<>(null);

    @ModifyVariable(method = "sendChatCommand(Ljava/lang/String;)V", at = @At("HEAD"), ordinal = 0, argsOnly = true)
    public String translateCommand(String command) {
        ParseResults<CommandSource> results = parse(command);
        if (!GoogleChatConfig.General.enabled || !GoogleChatConfig.Advanced.translateMessageArguments || !results.getExceptions().isEmpty()) return command;
        final boolean[] modified = {false};
        results.getContext().getArguments().replaceAll((key, value) -> {
            if (value.getResult() instanceof MessageArgumentType.MessageFormat fmt) {
                if (fmt.selectors().length != 0) return value; // Selectors contain position information, which this would break
                fmt = new MessageArgumentType.MessageFormat(GoogleChat.translateIfNeeded(fmt.contents(), TranslationDirection.C2S, true), new MessageArgumentType.MessageSelector[0]);
                modified[0] = true;
                return new ParsedArgument<>(value.getRange().getStart(), value.getRange().getEnd(), fmt);
            }
            return value;
        });
        if (!modified[0]) return command;
        return GoogleChatClient.reconstitute(command, results.getContext()).map(s -> {
            googlechat$lastResults.set(results);
            return s;
        }).orElse(command);
    }

    @Redirect(method = "sendChatCommand(Ljava/lang/String;)V", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/network/ClientPlayNetworkHandler;parse(Ljava/lang/String;)Lcom/mojang/brigadier/ParseResults;"))
    public ParseResults<CommandSource> googlechat$modifyArguments(ClientPlayNetworkHandler handler, String command) {
        ParseResults<CommandSource> results = googlechat$lastResults.getAndSet(null);
        if (results != null) return results;
        return parse(command);
    }
}
