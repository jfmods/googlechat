package io.gitlab.jfronny.googlechat.client.mixin;

import io.gitlab.jfronny.googlechat.GoogleChat;
import io.gitlab.jfronny.googlechat.TranslationDirection;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.text.Text;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyVariable;

@Mixin(ClientPlayerEntity.class)
public class ClientPlayerEntityMixin {
    @ModifyVariable(method = "sendMessage(Lnet/minecraft/text/Text;Z)V", at = @At("HEAD"), argsOnly = true, ordinal = 0)
    Text googlechat$translateMessage(Text source) {
        return GoogleChat.translateIfNeeded(source, TranslationDirection.C2S, true);
    }
}
