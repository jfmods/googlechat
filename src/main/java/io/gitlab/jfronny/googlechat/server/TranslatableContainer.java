package io.gitlab.jfronny.googlechat.server;

import io.gitlab.jfronny.googlechat.*;
import net.minecraft.text.Text;

import java.util.concurrent.CompletableFuture;

public sealed interface TranslatableContainer<T, S extends TranslatableContainer<T, S>> {
    S translate(TranslationDirection direction);

    record Sync(Text text) implements TranslatableContainer<Text, Sync> {
        @Override
        public Sync translate(TranslationDirection direction) {
            return new Sync(translateAndLog(text, direction));
        }
    }

    record Async(CompletableFuture<Text> text) implements TranslatableContainer<CompletableFuture<Text>, Async> {
        @Override
        public Async translate(TranslationDirection direction) {
            return new Async(text.thenApplyAsync(msg -> translateAndLog(msg, direction)));
        }
    }

    static Text translateAndLog(final Text source, final TranslationDirection direction) {
        var translated = GoogleChat.translateIfNeeded(source, direction, true);
        if (GoogleChatConfig.Advanced.debugLogs) GoogleChat.LOGGER.info("Applied C2S translation from {0} to {1}", source, translated);
        return translated;
    }
}
