package io.gitlab.jfronny.googlechat.server.mixin;

import com.mojang.brigadier.context.CommandContext;
import io.gitlab.jfronny.googlechat.GoogleChatConfig;
import io.gitlab.jfronny.googlechat.TranslationDirection;
import io.gitlab.jfronny.googlechat.server.TranslatableContainer;
import net.minecraft.command.argument.MessageArgumentType;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.Text;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(MessageArgumentType.class)
public class MessageArgumentTypeMixin {
    @Inject(method = "getMessage(Lcom/mojang/brigadier/context/CommandContext;Ljava/lang/String;)Lnet/minecraft/text/Text;", at = @At("TAIL"), cancellable = true)
    private static void modifyMessage(CommandContext<ServerCommandSource> context, String name, CallbackInfoReturnable<Text> cir) {
        if (!GoogleChatConfig.General.enabled || !GoogleChatConfig.Advanced.translateMessageArguments) return;
        Text message = cir.getReturnValue();
        if (context.getSource().getPlayer() != null) { // Client messages should first be translated to the server language
            if (TranslationDirection.C2S.hasTarget()) {
                if (TranslationDirection.S2C.hasTarget()) {
                    // Do not translate back and forth
                    return;
                }
            }
            message = TranslatableContainer.translateAndLog(message, TranslationDirection.C2S);
        }
        // All messages should be translated to the client language before sending
        message = TranslatableContainer.translateAndLog(message, TranslationDirection.S2C);
        cir.setReturnValue(message);
    }
}
