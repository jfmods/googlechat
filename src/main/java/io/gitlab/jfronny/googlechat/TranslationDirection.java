package io.gitlab.jfronny.googlechat;

import net.fabricmc.api.EnvType;
import net.fabricmc.loader.api.FabricLoader;

public enum TranslationDirection {
    C2S,
    S2C;

    public String source() {
        return switch (this) {
            case C2S -> GoogleChatConfig.General.clientLanguage;
            case S2C -> GoogleChatConfig.General.serverLanguage;
        };
    }

    public String target() {
        return switch (this) {
            case C2S -> GoogleChatConfig.General.serverLanguage;
            case S2C -> GoogleChatConfig.General.clientLanguage;
        };
    }

    public boolean hasTarget() {
        return !target().equals("auto");
    }

    public boolean shouldSkipOutright() {
        if (!GoogleChatConfig.General.enabled) return true;
        return !hasTarget();
    }

    public boolean failsRegex(String text) {
        boolean isSender = (FabricLoader.getInstance().getEnvironmentType() == EnvType.CLIENT) == (this == TranslationDirection.C2S);
        if (isSender) return text.matches(GoogleChatConfig.Processing.sendingRegex) == GoogleChatConfig.Processing.sendingRegexIsBlacklist;
        else return text.matches(GoogleChatConfig.Processing.receivingRegex) == GoogleChatConfig.Processing.receivingRegexIsBlacklist;
    }
}
